package fr.epsi.webshopapi.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.epsi.webshopapi.Exception.InvalidRequestException;
import fr.epsi.webshopapi.Exception.NotFoundException;
import fr.epsi.webshopapi.Model.Prospect;
import fr.epsi.webshopapi.Repository.ProspectRepository;

@Service
public class ProspectService {
    
    @Autowired
    private ProspectRepository prospectRepository;

    public Prospect getProspectById(Long id) { 
        return prospectRepository.findById(id).orElseThrow(() -> new NotFoundException("Prospect with id "+id+" not found!"));
    }

    public List<Prospect> getProspects() { 
        return prospectRepository.findAll();
    }

    public Prospect addProspect(Prospect prospect) { 
        return prospectRepository.save(prospect);
    }

    public Prospect updateProspect(Prospect prospect) {
        if (prospect == null || prospect.getId() == null) {
            throw new InvalidRequestException("Prospect or id must not be null!");
        }
        Prospect existingProspect = getProspectById(prospect.getId());
        existingProspect.setFirstName(prospect.getFirstName());
        existingProspect.setLastName(prospect.getLastName());
        existingProspect.setAddress(prospect.getAddress());
        existingProspect.setPhoneNumber(prospect.getPhoneNumber());
        prospectRepository.save(existingProspect);
        return existingProspect;
    }

    public void deleteProspect(Long id) { 
        Prospect prospect = getProspectById(id);
        prospectRepository.deleteById(prospect.getId());
    }
}
