package fr.epsi.webshopapi.Model;

import java.util.Date;
import java.util.List;

import org.springframework.data.annotation.CreatedDate;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Builder.Default;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Order {

    private Long id;

    private Long customerId;

    private List<Product> products;

    @CreatedDate @Default
    private Date createdAt = new Date();
}
