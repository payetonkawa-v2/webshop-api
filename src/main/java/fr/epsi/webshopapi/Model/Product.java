package fr.epsi.webshopapi.Model;

import java.util.Date;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Builder.Default;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class Product {
    
    private Long id;

    private String name;

    private String libelle;

    private String price;

    private String description;

    private String color;

    private int stock;

    @CreatedDate @Default
    private Date createdAt = new Date();
    
    @LastModifiedDate @Default
    private Date updatedAt = new Date();

}
