package fr.epsi.webshopapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WebshopapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(WebshopapiApplication.class, args);
	}

}
